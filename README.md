## OPEN ENT NG prerequisites

- LyStore installed from maven.web-education and github/OPEN-ENT-NG repositories
- 3 structures needed :
  - 0782593V - LP JEAN PERRIN - SAINT-CYR-L'ECOLE
  - 0910715H - LP LYC METIER JEAN PERRIN - LONGJUMEAU
  - 0951104J - LPO JEAN PERRIN - SAINT-OUEN-L'AUMONE
- groups "Lystore", "Lystore - Administrateurs", "Lystore - Correspondants région" deployed with correct roles defined
- 3 accounts pascal.fautrero/azerty123, user.manager/azerty123, patrick.severac/azerty123 (defined in behat.yml)
- pascal.fautrero recorded in group Lystore admin
- patrick.severac recorded in group Lystore in 0951104J
- Init database with some specific data :

  INSERT INTO lystore.contract_type VALUES (1, '236', 'Subventions')
  INSERT INTO lystore.contract_type VALUES (2, '21828', 'Véhicules')
  INSERT INTO lystore.contract_type VALUES (3, '21831', 'Informatique')
  INSERT INTO lystore.contract_type VALUES (4, '21841', 'Mobilier')
  INSERT INTO lystore.tax VALUES (1, 'TVA_20', 20)
  INSERT INTO lystore.program VALUES (1, 'HP222-008')
  INSERT INTO lystore.program VALUES (2, 'HP224-013')


## Installation

```
# Retrieve lystore test suite
sudo apt-get install git wget curl
git clone https://gitlab.com/polelycees/lystoreTests.git
cd lystoreTests
# install php
sudo apt-get install php php-mbstring php-curl php-xml
# retrieve composer and selenium locally
curl http://getcomposer.org/installer | php
curl http://selenium-release.storage.googleapis.com/3.8/selenium-server-standalone-3.8.1.jar > selenium.jar
# install behat & mink extension & selenium driver
php composer.phar require --dev behat/mink-extension
php composer.phar require --dev behat/mink-selenium2-driver
php composer.phar require --dev smalot/pdfparser
# install geckodriver (just for information)
wget https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-linux64.tar.gz
tar -xvzf geckodriver-v0.20.1-linux64.tar.gz
ls -la
chmod +x geckodriver
sudo mv geckodriver /usr/local/bin/

#install Chrome & ChromeDriver
cd ~
wget -N https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip -P ~/
unzip ~/chromedriver_linux64.zip -d ~/
rm ~/chromedriver_linux64.zip
sudo chmod +x chromedriver
sudo mv -f ~/chromedriver /usr/local/bin/chromedriver
sudo su -
curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
apt-get -y update
apt-get -y install google-chrome-stable
exit

```

## Usage

Fire Selenium2 (once):

`java -jar selenium.jar -log /tmp/selenium.log &`

Launch Test suite (as many time as needed):

`vendor/bin/behat --stop-on-failure --strict`
