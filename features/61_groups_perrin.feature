Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Campagnes"
    And I click on Menu item "redirectTo('/structureGroups')"
    And I wait for "2"
    Then I should see "Gestion des regroupements d'établissements"
    When I press add group button
    And I wait for "2"
    Then I should see "Ajouter un regroupement d'établissements"
    When I fill ent input "structureGroup.name" with "Lycées Campagne équipements numériques 2018"
    And I fill ent textarea "structureGroup.description" with:
    """
    Ensemble des lycées d'île de France
    """
    And I fill ent input "search.structure" with "PERRIN"
    And I wait for "2"
    And I click on the text containing "0782593V"
    And I click on the text containing "0910715H"
    And I click on the text containing "0951104J"
    And I open lightbox with "addStructuresInGroup()"
    And I wait for "2"
    And I click on submit input "validStructureGroup(structureGroup)"
    And I wait for "2"
