Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Campagnes"
    And I click on Menu item "redirectTo('/structureGroups')"
    And I wait for "2"
    Then I should see "Gestion des regroupements d'établissements"
    When I press add group button
    And I wait for "2"
    Then I should see "Ajouter un regroupement d'établissements"
    When I fill ent input "structureGroup.name" with "Lycées 100% numérique"
    And I fill ent textarea "structureGroup.description" with:
    """
    Lycées 100% numérique
    qui auront accès à certains équipements
    """
    And I click on "li[class='ellipsis ng-binding ng-scope']"
    And I open lightbox with "addStructuresInGroup()"
    And I wait for "2"
    And I click on submit input "validStructureGroup(structureGroup)"
    And I wait for "2"
    Then I should see "Lycées 100% numérique"

    When I press add group button
    And I wait for "2"
    When I fill ent input "structureGroup.name" with "Tous les lycées d'île-de-France"
    And I fill ent textarea "structureGroup.description" with:
    """
    Ensemble des lycées d'île de France
    """
    And I click on "li[class='ellipsis ng-binding ng-scope']"
    And I open lightbox with "addStructuresInGroup()"
    And I wait for "2"
    And I click on submit input "validStructureGroup(structureGroup)"
    And I wait for "2"
