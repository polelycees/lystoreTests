Feature: User part of lyStore


@javascript
Scenario: I am a user manager
  Given I am on "/lystore"
  And I wait for "2"
  When I log in as "manager"
  And I wait for "2"

  When I click on dropdownmenu "Commandes"
  And I click on Menu item "redirectTo('/order/valid')"
  And I wait for "2"

  Then I should see "Générer les bons de commande"
  And pdf file 'BC_BC01.pdf' should contain "MARCHE N° : 154300-01"
