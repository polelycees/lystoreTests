Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Marchés"
    And I click on Menu item "redirectTo('/agents')"
    And I wait for "2"

    And I open lightbox with "openAgentForm()"
    And I wait for "2"
    When I fill ent input "agent.name" with "Nelly Bardoux"
    And I fill ent input "agent.department" with "SAJ / DPEE / Pôle Lycées"
    And I fill ent input "agent.phone" with "01 53 85 58 36"
    And I fill ent input "agent.email" with "nelly.bardoux@iledefrance.fr"
    And I submit form "validAgent(agent)"
    And I wait for "2"

    When I open lightbox with "openAgentForm()"
    And I wait for "2"
    When I fill ent input "agent.name" with "Nathalie Astori"
    And I fill ent input "agent.department" with "SAJ / DPEE / Pôle Lycées"
    And I fill ent input "agent.phone" with "01 53 85 58 35"
    And I fill ent input "agent.email" with "nathalie.astori@iledefrance.fr"
    And I submit form "validAgent(agent)"
    And I wait for "2"

    When I open lightbox with "openAgentForm()"
    And I wait for "2"
    When I fill ent input "agent.name" with "Laetitia Djian"
    And I fill ent input "agent.department" with "SAJ / DPEE / Pôle Lycées"
    And I fill ent input "agent.phone" with "01 53 85 57 12"
    And I fill ent input "agent.email" with "laetitia.djian@iledefrance.fr"
    And I submit form "validAgent(agent)"
    And I wait for "2"
