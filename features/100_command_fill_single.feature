Feature: User part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "user"
    And I wait for "2"
    Then I should see "Plan prévisionnel d'équipements numériques 2018"

    When I click on the text "Plan prévisionnel d'équipements numériques 2018"
    And I wait for "2"

    When I click on the text "Unité centrale compacte"
    And I wait for "2"
    Then I should see "Unité centrale compacte"
    Then I fill ent input "basket.amount" with "11"
    And I click on submit input "addBasketItem(basket)"
    And I wait for "5"
    And I click on header "openBasket()"
    And I wait for "2"
    Then I should see "Unité centrale compacte"

    When I click on header "openBasket()"
    And I wait for "2"
    Then I should see "Unité centrale compacte"

    When I click on input button "Passer la commande"
    And I wait for "2"
    Then I should see "Commande effectuée avec succès"

    When I click on input button "ok"
    Then I should see "Le panier est vide"
    And I wait for "2"

    When I click on header "openOrder()"
    And I wait for "2"
    Then I should see "Unité centrale compacte"

    When I delete command "Unité centrale compacte"
