Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Campagnes"
    And I click on Menu item "redirectTo('/campaigns')"
    And I wait for "2"
    When I click on submit input "openCampaignForm()"
    And I wait for "2"
    Then I should see "Ajouter une campagne"
    When I fill ent input "campaign.name" with "Plan prévisionnel d'équipements numériques 2018"
    And I select group "Lycées Campagne équipements numériques 2018"
    And I select tag "ordinateur" for group "Lycées Campagne équipements numériques 2018"
    And I select tag "imprimante" for group "Lycées Campagne équipements numériques 2018"
    And I select tag "vidéoprojecteur" for group "Lycées Campagne équipements numériques 2018"
    And I click on submit input "validCampaign(campaign)"
    And I wait for "2"
    Then I should see "Plan prévisionnel d'équipements numériques 2018"
