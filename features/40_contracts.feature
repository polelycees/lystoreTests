Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Marchés"
    And I click on Menu item "redirectTo('/contracts')"
    And I wait for "2"
    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché PC/MAC lot1"
    And I fill ent input "contract.reference" with "154300-01"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    Then I should see "2010"
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    Then I should see "janv."
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Nelly Bardoux" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "Econocom" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"

    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché PC/MAC lot2"
    And I fill ent input "contract.reference" with "154300-02"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    Then I should see "2010"
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    Then I should see "janv."
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Nelly Bardoux" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "Econocom" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"

    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché Imprimante usage bureautique lot1"
    And I fill ent input "contract.reference" with "1700146-01"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    Then I should see "2010"
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    Then I should see "janv."
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Nathalie Astori" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "CFI" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"

    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché Imprimante usage département lot2"
    And I fill ent input "contract.reference" with "1700146-02"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Nathalie Astori" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "Econocom" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"

    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché VP lot1"
    And I fill ent input "contract.reference" with "1600455-01"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Laetitia Djian" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "Signal" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"

    And I open lightbox with "openContractForm()"
    And I wait for "2"
    When I fill ent input "contract.name" with "Marché VP lot2"
    And I fill ent input "contract.reference" with "1600455-02"
    And I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "datepicker-months" switcher
    And I click on "span:contains('2010')"
    When I click on input "contract.start_date"
    And I click on "datepicker-days" switcher
    And I click on "span:contains('janv.')"
    And I fill ent input "contract.nb_renewal" with "10"
    When I click on select "contract.id_contract_type"
    And I select "21831 - Informatique" from ent "contract.id_contract_type"
    When I click on select "contract.id_agent"
    And I select "Laetitia Djian" from ent "contract.id_agent"
    When I click on select "contract.id_supplier"
    And I select "Signal" from ent "contract.id_supplier"
    And I submit form "validContract(contract)"
    And I wait for "2"
