Feature: User part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "user"
    And I wait for "2"
    Then I should see "Plan prévisionnel d'équipements numériques 2018"

    When I click on the text "Plan prévisionnel d'équipements numériques 2018"
    And I wait for "2"
    Then I should see "catalogue"
    And I should see "Suivi des commandes"
    And I should see "panier"
    And I should see "cagnotte"

    When I click on header "openOrder()"
    And I wait for "2"
    Then I should see "Vous n'avez aucune commande en cours"

    When I click on header "openBasket()"
    And I wait for "2"
    Then I should see "Le panier est vide"

    When I click on header "openCatalog()"
    And I wait for "2"
    Then I should see "Unité centrale compacte"

    When I click on the text "Unité centrale compacte"
    And I wait for "2"
    Then I should see "Unité centrale compacte"
    Then I fill ent input "basket.amount" with "11"
    And I click on submit input "addBasketItem(basket)"
    And I wait for "5"
    And I click on header "openBasket()"
    And I wait for "2"
    Then I should see "Unité centrale compacte"

    When I click on submit input "displayLightboxDelete(basket)"
    And I wait for "2"
    And I click on submit input "deleteBasket(basketToDelete)"
    And I wait for "2"
    Then I should see "Le panier est vide"
