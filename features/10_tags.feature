Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    Then I should see "CONNEXION"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Équipements"
    And I click on Menu item "redirectTo('/tags')"
    And I wait for "2"
    Then I should see "Ajouter un label"

    When I open lightbox with "openTagForm()"
    And I wait for "2"
    When I fill ent input "tag.name" with "portable"
    And I select color "rgb(68, 173, 142)"
    And I submit form "validTag(tag)"
    And I wait for "2"

    When I open lightbox with "openTagForm()"
    And I wait for "2"
    When I fill ent input "tag.name" with "ordinateur"
    And I select color "rgb(68, 173, 142)"
    And I submit form "validTag(tag)"
    And I wait for "2"

    When I open lightbox with "openTagForm()"
    And I wait for "2"
    When I fill ent input "tag.name" with "imprimante"
    And I select color "rgb(68, 173, 142)"
    And I submit form "validTag(tag)"
    And I wait for "2"

    When I open lightbox with "openTagForm()"
    And I wait for "2"
    When I fill ent input "tag.name" with "vidéoprojecteur"
    And I select color "rgb(68, 173, 142)"
    And I submit form "validTag(tag)"
    And I wait for "2"
