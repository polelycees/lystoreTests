<?php

// features/bootstrap/FeatureContext.php

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;

class FeatureContext extends RawMinkContext implements Context
{
  public function __construct($admin, $user, $manager)
  {
    $this->admin = $admin;
    $this->user = $user;
    $this->manager = $manager;
  }

  /** @BeforeFeature */
  public static function prepareForTheFeature()
  {
    // clean database or do other preparation stuff

  }

  /** @Given we have some context */
  public function prepareContext()
  {
    // do something

  }

  /**
    * @return \Behat\Mink\Element\DocumentElement
    */
   private function getPage()
    {
      return $this->getSession()->getPage();
    }


  /**
   * @Given I wait for :sec
   **/
  public function iWaitFor($sec)
  {
    sleep($sec);
  }

  /**
   * @Given I click on dropdownmenu :entry
   **/
  public function iClickOnDropdownMenu($entry)
  {
    $button = $this->getPage()->find('css', '[title~="' . $entry . '"]');
    if (null === $button) {
        throw new \Exception('The element is not found');
    }
    $button->press();

  }

  /**
   * @Given I click on Menu Item :menuitem
   **/
  public function iClickOnMenuItem($menuitem)
  {
    $button = $this->getPage()->find('css', 'opt[ng-click="' . $menuitem . '"]');
    if (null === $button) {
        throw new \Exception('The element is not found');
    }
    $button->press();
  }


    /**
     * @Given I click on header :menuitem
     **/
    public function iClickOnHeader($menuitem)
    {
      $button = $this->getPage()->find('css', 'header[ng-click="' . $menuitem . '"]');
      if (null === $button) {
          throw new \Exception('The element is not found');
      }
      $button->click();
    }



  /**
   * @Given I click on submit input :menuitem
   **/
  public function iClickOnSubmitInput($menuitem)
  {
    $button = $this->getPage()->find('css', 'input[ng-click="' . $menuitem . '"]');
    if (null === $button) {
        throw new \Exception('The element is not found');
    }
    $button->click();
  }

  /**
   * @When I delete command :arg1
   */
  public function iDeleteCommand($arg1)
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[contains(text(),'$arg1')]/../../..//input[@value='Supprimer']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    $element->click();
    sleep(2);
    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          '//input[@ng-click="deleteOrderEquipment(orderEquipmentToDelete)"]'
        )
    );
    $element->click();
  }



  /**
   * @Given I open lightbox with :ngclick
   **/
  public function iClickOnLightboxSupplier($ngclick)
  {
    $this->getSession()->executeScript('window.scrollTo(0,0);');
    $button = $this->getPage()->find('css', 'button[ng-click="' . $ngclick . '"]');

    if (null === $button) {
        throw new \Exception('The element is not found');
    }
    $button->press();
  }

  /**
   * @Given I press add group button
   **/
  public function iPressAddGroupButton()
  {
    $this->getSession()->executeScript('window.scrollTo(0,0);');
    $button = $this->getPage()->find('css', 'i[ng-click="openStructureGroupForm()"]');

    if (null === $button) {
        throw new \Exception('The element is not found');
    }
    $button->press();
  }

  /**
   * @Given I fill ent input :ngmodel with :value
   **/
  public function iFillEntInput($ngmodel, $value)
  {
    $input = $this->getPage()->find('css', 'input[ng-model="' . $ngmodel . '"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->setValue($value);
  }

  /**
   * @Given I fill editor area :ngmodel with:
   **/
  public function iFillEditorArea($ngmodel, $value)
  {
    $input = $this->getPage()->find('css', 'editor[ng-model="' . $ngmodel . '"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->setValue($value);
  }



    /**
     * @When I log in as :user
     **/

    public function iLogInAs($user)
    {
      $element = $this->getPage()->find(
          'xpath',
          $this->getSession()->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//input[@id='email']"
          )
      );
      if (null === $element) {
          throw new \Exception('The element is not found');
      }
      $element->setValue($this->{$user}['name']);

      $element = $this->getPage()->find(
          'xpath',
          $this->getSession()->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//input[@id='password']"
          )
      );
      if (null === $element) {
          throw new \Exception('The element is not found');
      }
      $element->setValue($this->{$user}['password']);

      $element = $this->getPage()->find(
          'xpath',
          $this->getSession()->getSelectorsHandler()->selectorToXpath(
            'xpath',
            '//*[text()="Connexion"]/ancestor::button'
          )
      );

      if (null === $element) {
          throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
      }
      $element->click();

    }


  /**
   * @When I fill input :ngmodel sibling :position with :value
   **/

  public function iFillEntInput2($ngmodel, $position, $value)
  {
    $input = $this->getPage()->findAll('css', 'input[ng-model="' . $ngmodel . '"]')[$position];
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->setValue($value);
  }


  /**
   * @When I select group :group
   **/

  public function iSelectGroup($group)
  {
    $session = $this->getSession();
    $this->getSession()->executeScript('window.scrollTo(0,1000);');
    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[contains(text(),'$group')]/..//label[@class='checkbox']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    $element->click();
    sleep(1);

  }

    /**
     * @When I select tag :tag for group :group
     **/

    public function iSelectTagForGroup($tag, $group)
    {
      $session = $this->getSession();

      $buttonPlus = $this->getPage()->find(
          'xpath',
          $session->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//*[contains(text(),'$group')]/..//input[@ng-click='display.input.group[\$index]=true']"
          )
      );
      if (null === $buttonPlus) {
          throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
      }
      $buttonPlus->click();
      sleep(1);

      $searchInput = $this->getPage()->find(
          'xpath',
          $session->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//*[contains(text(),'$group')]/..//input[@ng-model='search']"
          )
      );
      if (null === $searchInput) {
          throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
      }

      $elementToSelect = $tag;
      $buttonPlus->click();
      sleep(1);
      $searchInput->setValue($elementToSelect);
      sleep(1);
      $element = $this->getPage()->find(
          'xpath',
          $session->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//li[text()='$tag']"
          )
      );
      if (null === $element) {
          throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
      }
      $element->click();
    }





  /**
      * Click some text
      *
      * @When /^I click on the text "([^"]*)"$/
      */
      public function iClickOnTheText($text)
      {
          $session = $this->getSession();
          $element = $session->getPage()->find(
              'xpath',
              $session->getSelectorsHandler()->selectorToXpath('xpath', '//*[.="'. $text .'"]')
          );
          if (null === $element) {
              throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
          }

          $element->click();
      }


        /**
            * Click some text
            *
            * @When I click on the text containing :text
            */
            public function iClickOnTheTextContaining($text)
            {
                $session = $this->getSession();
                $element = $session->getPage()->find(
                    'xpath',
                    $session->getSelectorsHandler()->selectorToXpath('xpath', '//*[contains(text(),"'.$text.'")]')
                );
                if (null === $element) {
                    throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
                }

                $element->click();
            }

      /**
       * @When I click on manage purse from :arg1
       */
      public function iClickOnManagePurseFrom($arg1)
      {
        $session = $this->getSession();
        //$this->getSession()->executeScript('window.scrollTo(0,1000);');
        $element = $this->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath(
              'xpath',
              '//*[contains(text(),"'.$arg1.'")]/..//span[contains(text(),"Gérer les cagnottes")]'
            )
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
        }

        $element->click();
      }

      /**
       * @Given I click on button :arg1
       */
      public function iClickOnButton($arg1)
      {
        $session = $this->getSession();
        $element = $this->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath(
              'xpath',
              '//*[text()="'.$arg1.'"]/ancestor::button'
            )
        );

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
        }
        $element->click();
      }

      /**
       * @Given I click on input button :arg1
       */
      public function iClickOnInputButton($arg1)
      {
        $session = $this->getSession();
        $element = $this->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath(
              'xpath',
              '//input[@value="'.$arg1.'"]'
            )
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
        }
        $element->click();
      }



      /**
       * @When I attach file :arg1
       */
      public function iAttachFile($arg1)
      {
        $session = $this->getSession();
        $element = $this->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath(
              'xpath',
              "//input[@ng-model='importer.files']"
            )
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
        }
        if ($this->getMinkParameter('files_path')) {
          $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$arg1;
          if (is_file($fullPath)) {
            $arg1 = $fullPath;
          }
        }

        $element->attachFile($arg1);

      }



  /**
      * Click some text
      *
      * @When I click on header text :text
      */
      public function iClickOnHeaderText($text)
      {
          $session = $this->getSession();
          $element = $session->getPage()->find(
              'xpath',
              $session->getSelectorsHandler()->selectorToXpath('xpath', '//span[.="'. $text .'"]')
          );
          if (null === $element) {
              throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
          }

          $element->click();
      }


  /**
   * @Given I click on ent checkbox with :value
   **/
  public function iClickOnENTCheckbox($value)
  {
    $input = $this->getPage()->find('css', 'input[value="' . $value . '"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->click();
  }


  /**
   * @Given I click on input :ngmodel
   **/
  public function iclickOnInput($ngmodel)
  {
    $input = $this->getPage()->find('css', 'input[ng-model="' . $ngmodel . '"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->click();
  }

  /**
   * @Given I click on :type switcher
   **/
  public function iclickOnDatepickerSwitcher($type)
  {
    $input = $this->getPage()->find('css', 'div[class="' . $type . '"] th[class="switch"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->click();
  }

  /**
   * @Given /^I click on "([^"]*)"$/
   */
  public function iClickOn($element)
  {
      $page = $this->getSession()->getPage();
      $findName = $page->find("css", $element);
      if (!$findName) {
          throw new Exception($element . " could not be found");
      } else {
          $findName->click();
      }
  }


  /**
     *
     * @Given I select :option from ent :select
     */
    public function selectEntOption($select, $option)
    {
        $select = $this->getPage()->find('css', 'select[ng-model="' . $select . '"]');
        $option = $this->getPage()->find('css', 'option[label="' . $option . '"]');
        $select->click();
        $option->click();
    }



  /**
   * @Given I click on select :ngmodel
   **/
  public function iclickOnSelect($ngmodel)
  {
    $select = $this->getPage()->find('css', 'select[ng-model="' . $ngmodel . '"]');
    if (null === $select) {
        throw new \Exception('The element is not found');
    }
    $select->click();
  }


  /**
   * @Given I click on label :ngmodel
   **/
  public function iclickOnLabel($ngmodel)
  {
    $input = $this->getPage()->find('css', 'input[ng-change="switchAllSupplier(allSupplierSelected)"]');
    if (null === $input) {
        throw new \Exception('The element is not found');
    }
    $input->click();
  }

  /**
   * @Given I fill ent textarea :ngmodel with:
   **/
  public function iFillEntTextarea($ngmodel, $value)
  {
    $textarea = $this->getPage()->find('css', 'textarea[ng-model="' . $ngmodel . '"]');
    if (null === $textarea) {
        throw new \Exception('The element is not found');
    }
    $textarea->setValue($value);
  }


  /**
   * @Given I select color :color
   **/
  public function iSelectColor($color)
  {
    $item = $this->getPage()->find('css', 'span[ng-click="tag.color = color"][style="background-color: ' . $color . ';"]');

    if (null === $item) {
        throw new \Exception('The element is not found');
    }

    $item->click();
  }

  /**
   * @Given I should see color :color
   **/
  public function iShouldSeeColor($color)
  {
    $item = $this->getPage()->find('css', 'span[style="background-color: ' . $color . ';"]');
    if (null === $item) {
        throw new \Exception('The element is not found');
    }
    if ($item->isVisible()) {
      return;
    }
    else {
      throw new \Exception("Color \"$color\" not visible.");
    }
  }



 //rgb(68, 173, 142);


  /**
   * @Given I submit form :ngsubmit
   **/
  public function iSubmitForm($ngsubmit)
  {
    $form = $this->getPage()->find('css', 'form[ng-submit="' . $ngsubmit . '"]');
    if (null === $form) {
        throw new \Exception('The element is not found');
    }
    $form->submit();
  }

  /**
   * @Then I select command for :arg1
   */
  public function iSelectCommandFor($arg1)
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[contains(text(),'$arg1')]/..//label[@class='checkbox']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    $element->click();
  }

  /**
   * @Then I should see :arg1 greyed
   */
  public function iShouldSeeGreyed($arg1)
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[contains(text(),'$arg1')]/.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    if (!$element->hasClass("SENT")) {
        throw new \InvalidArgumentException(sprintf('Cannot find class'));
    }
  }



    /**
     * @Then I select all commands
     */
    public function iSelectAllCommands()
    {
      $session = $this->getSession();

      $element = $this->getPage()->find(
          'xpath',
          $session->getSelectorsHandler()->selectorToXpath(
            'xpath',
            "//*[contains(text(),'Établissement')]/../../..//label[@class='checkbox']"
          )
      );
      if (null === $element) {
          throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
      }
      $element->click();
    }


  /**
   * @Then I validate this command
   */
  public function iValidateThisCommand()
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Valider']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    $element->click();

  }

  /**
   * @Then I generate bc for this command
   */
  public function iGenerateBCForThisCommand()
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Valider BC']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException('Cannot find "Valider BC"');
    }
    $element->click();

  }

  /**
   * @When pdf file :arg1 should contain :txt
   */
  public function icheckPDF($arg1, $txt)
  {
    // Parse pdf file and build necessary objects.
    $parser = new \Smalot\PdfParser\Parser();
    $uid = posix_getuid();
    $shell_user = posix_getpwuid($uid);
    $pdf    = $parser->parseFile($shell_user['dir'] . '/Téléchargements/' . $arg1);
    $text = $pdf->getText();
    if (strpos($text, $txt) === false) {
        throw new \Exception($txt . ' not found');
    }
  }


  /**
   * @When I upload image :arg1
   */
  public function iUploadImage($arg1)
  {
    $session = $this->getSession();

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//img[@class='pick-file ng-scope']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find pick-file'));
    }
    $element->click();
    sleep(2);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Charger un document']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text - Charger un document -'));
    }
    $element->click();
    sleep(2);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//input[@ng-model='upload.files']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text upload.files'));
    }
    if ($this->getMinkParameter('files_path')) {
      $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$arg1;
      if (is_file($fullPath)) {
        $arg1 = $fullPath;
      }
    }

    $element->attachFile($arg1);
    sleep(5);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Importer']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text - Importer - '));
    }
    $element->click();
    sleep(2);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Documents publics']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text - Documents publics -'));
    }
    $element->click();

    sleep(2);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='".basename($arg1)."']"
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $arg1));
    }
    $element->click();
    sleep(2);

    $element = $this->getPage()->find(
        'xpath',
        $session->getSelectorsHandler()->selectorToXpath(
          'xpath',
          "//*[text()='Ajouter']/../.."
        )
    );
    if (null === $element) {
        throw new \InvalidArgumentException(sprintf('Cannot find text - Ajouter - '));
    }
    $element->click();
  }
}
