Feature: User part of lyStore

  @javascript
  Scenario: I am a user manager
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "manager"
    And I wait for "2"

    When I click on dropdownmenu "Commandes"
    And I click on Menu item "redirectTo('/order/valid')"
    And I wait for "2"

    Then I should see "Générer les bons de commande"
    And I should see "Marché PC/MAC lot1"
    And I select command for "Marché PC/MAC lot1"
    And I wait for "2"
    And I generate bc for this command

    And I wait for "2"
    And I fill ent input "orderToSend.bc_number" with "BC01"
    And I fill ent input "orderToSend.engagement_number" with "E001"
    And I click on select "orderToSend.id_program"
    And I select "HP222-008" from ent "orderToSend.id_program"
    And I click on input button "Valider"
    And I wait for "2"
    Then I should see "Vous êtes sur le point de valider définitivement ces commandes"
    And I should see "Numéro du Bon de Commande : BC01"
    And I should see "Numéro d'engagement : E001"
    And I should see "Programme concerné : HP222-008"
    And I click on submit input "sendOrders(orderToSend)"
    And I wait for "2"
    Then I should see "Marché PC/MAC lot1" greyed
