Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Marchés"
    And I click on Menu item "redirectTo('/suppliers')"
    And I wait for "2"


    And I open lightbox with "openSupplierForm()"
    And I wait for "2"
    When I fill ent input "supplier.name" with "Econocom"
    And I fill ent textarea "supplier.address" with:
    """
    40 quai de Dion Bouton
    92800 Puteaux
    """
    And I fill ent input "supplier.phone" with "xx xx xx xx xx"
    And I fill ent input "supplier.email" with "xxx@xxx"
    And I submit form "validSupplier(supplier)"
    And I wait for "2"

    And I open lightbox with "openSupplierForm()"
    And I wait for "2"
    When I fill ent input "supplier.name" with "CFI"
    And I fill ent textarea "supplier.address" with:
    """
    Carré Pleyel 2, CALLIOPE
    5-7  rue Pleyel, CS40006
    93283 Saint-Denis Cedex
    """
    And I fill ent input "supplier.phone" with "xx xx xx xx xx"
    And I fill ent input "supplier.email" with "xxx@xxx"
    And I submit form "validSupplier(supplier)"
    And I wait for "2"

    And I open lightbox with "openSupplierForm()"
    And I wait for "2"
    When I fill ent input "supplier.name" with "Signal"
    And I fill ent textarea "supplier.address" with:
    """
    5 rue de Chaumont
    lieu-dit le Bois-Dieu
    78125 Hermeray
    """
    And I fill ent input "supplier.phone" with "xx xx xx xx xx"
    And I fill ent input "supplier.email" with "xxx@xxx"
    And I submit form "validSupplier(supplier)"
    And I wait for "2"
