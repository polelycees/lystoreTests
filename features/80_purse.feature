Feature: Admin part of lyStore

  @javascript
  Scenario: I am an administrator
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "admin"
    And I wait for "2"

    When I click on dropdownmenu "Campagnes"
    And I click on Menu item "redirectTo('/campaigns')"
    And I wait for "2"
    Then I should see "Plan prévisionnel d'équipements numériques 2018"
    When I click on manage purse from "Plan prévisionnel d'équipements numériques 2018"
    And I wait for "2"
    And I click on button "Import CSV"
    And I wait for "2"
    And I attach file "assets/purse.csv"
    And I click on button "Importer"

  @javascript
  Scenario: I am Patrick severac
    Given I am on "/lystore"
    And I wait for "2"
    When I log in as "user"
    And I wait for "2"
    Then I should see "Plan prévisionnel d'équipements numériques 2018"
    And I should see " Montant disponible : 50 000,00 €"
